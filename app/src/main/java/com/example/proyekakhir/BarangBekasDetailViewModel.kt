package com.example.proyekakhir

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import java.io.File
import java.util.*

class BarangBekasDetailViewModel() : ViewModel() {

    private val barangBekasRepository = BarangBekasRepository.get()
    private val barangBekasIdLiveData = MutableLiveData<UUID>()

    var barangBekasLiveData: LiveData<BarangBekas?> =
        Transformations.switchMap(barangBekasIdLiveData) { barangBekasId ->
            barangBekasRepository.getBarangBekas(barangBekasId)
        }

    fun loadBarangBekas(barangBekasId: UUID) {
        barangBekasIdLiveData.value = barangBekasId
    }

    fun saveBarangBekas(barangBekas: BarangBekas) {
        barangBekasRepository.updateBarangBekas(barangBekas)
    }

    fun getPhotoFile(barangBekas: BarangBekas): File {
        return barangBekasRepository.getPhotoFile(barangBekas)
    }
}