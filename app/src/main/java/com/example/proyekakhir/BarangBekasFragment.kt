package com.example.proyekakhir

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.EditText
import android.widget.ImageButton
import android.widget.ImageView
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.fragment_barang_bekas.view.*
import java.io.File
import java.util.*

private const val TAG = "BarangBekasFragment"
private const val ARG_BARANG_BEKAS_ID = "barangBekas_id"
private const val REQUEST_PHOTO = 2

class BarangBekasFragment : Fragment() {
    private lateinit var barangBekas: BarangBekas
    private lateinit var namaBarangField: EditText
    private lateinit var detailBarangField: EditText
    private lateinit var habisCheckBox: CheckBox
    private lateinit var photoButton : ImageButton
    private lateinit var photoView : ImageView
    private lateinit var photoFile: File
    private lateinit var photoUri: Uri

    private val barangBekasDetailViewModel: BarangBekasDetailViewModel by lazy {
        ViewModelProviders.of(this).get(BarangBekasDetailViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        barangBekas = BarangBekas()
        val barangBekasId: UUID = arguments?.getSerializable(ARG_BARANG_BEKAS_ID) as UUID
//        Log.d(TAG, "args bundle barangBekas ID: $barangBekasId")
        //Eventually, load crime from database
        barangBekasDetailViewModel.loadBarangBekas(barangBekasId)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_barang_bekas, container, false)

        namaBarangField = view.findViewById(R.id.nama_barang) as EditText
        detailBarangField = view.findViewById(R.id.detail_barang) as EditText
        habisCheckBox = view.findViewById(R.id.stok_habis) as CheckBox
        photoButton = view.findViewById(R.id.barang_bekas_camera) as ImageButton
        photoView = view.findViewById(R.id.barang_bekas_photo) as ImageView

        return view
    }

    private fun updateUI() {
        namaBarangField.setText(barangBekas.nama_barang)
        detailBarangField.setText(barangBekas.detail_barang)
//        habisCheckBox.isChecked = barangBekas.habis
        habisCheckBox.apply {
            isChecked = barangBekas.habis
            jumpDrawablesToCurrentState()
        }
        updatePhotoView()
    }

    private fun updatePhotoView() {
        if (photoFile.exists()) {
            val bitmap = getScaledBitmap(photoFile.path, requireActivity())
            photoView.setImageBitmap(bitmap)
        } else {
            photoView.setImageDrawable(null)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when {
            resultCode != Activity.RESULT_OK -> return
            requestCode == REQUEST_PHOTO -> {
                requireActivity().revokeUriPermission(photoUri,
                    Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
                updatePhotoView()
            }
        }
    }


    override fun onStart() {
        super.onStart()
        val namaBarangWatcher = object : TextWatcher {
            override fun beforeTextChanged(
                sequence: CharSequence?,
                start: Int,
                count: Int,
                after: Int) {
                //TODO("not implemented")
            }

            override fun onTextChanged(
                sequence:CharSequence?,
                start: Int,
                before: Int,
                count: Int) {
                barangBekas.nama_barang = sequence.toString()
            }

            override fun afterTextChanged(s: Editable?) {
                // TODO("not implemented")
            }
        }

        val detailWatcher = object : TextWatcher {
            override fun beforeTextChanged(
                sequence: CharSequence?,
                start: Int,
                count: Int,
                after: Int) {
                //TODO("not implemented")
            }

            override fun onTextChanged(
                sequence:CharSequence?,
                start: Int,
                before: Int,
                count: Int) {
                barangBekas.detail_barang = sequence.toString()
            }

            override fun afterTextChanged(s: Editable?) {
                // TODO("not implemented")
            }
        }

        namaBarangField.addTextChangedListener(namaBarangWatcher)
        detailBarangField.addTextChangedListener(detailWatcher)

        habisCheckBox.apply {
            setOnCheckedChangeListener { _, isChecked ->
                barangBekas.habis = isChecked
            }
        }

        photoButton.apply {
            val packageManager: PackageManager = requireActivity().packageManager

            val captureImage = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            val resolvedActivity: ResolveInfo? =
                packageManager.resolveActivity(captureImage,
                    PackageManager.MATCH_DEFAULT_ONLY)
            if (resolvedActivity == null){
                isEnabled = false
            }
            setOnClickListener {
                captureImage.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
                val cameraActivities: List<ResolveInfo> =
                    packageManager.queryIntentActivities(captureImage,
                        PackageManager.MATCH_DEFAULT_ONLY)

                for (cameraActivity in cameraActivities) {
                    requireActivity().grantUriPermission(
                        cameraActivity.activityInfo.packageName,
                        photoUri,
                        Intent.FLAG_GRANT_WRITE_URI_PERMISSION
                    )
                }
                startActivityForResult(captureImage, REQUEST_PHOTO)
            }
        }
    }

    override fun onStop() {
        super.onStop()
        barangBekasDetailViewModel.saveBarangBekas(barangBekas)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        barangBekasDetailViewModel.barangBekasLiveData.observe(
            viewLifecycleOwner,
            Observer { barangBekas ->
                barangBekas?.let {
                    this.barangBekas = barangBekas
                    photoFile = barangBekasDetailViewModel.getPhotoFile(barangBekas)
                    photoUri = FileProvider.getUriForFile(
                        requireActivity(),
                        "com.example.proyekakhir.fileprovider",
                        photoFile
                    )
                    updateUI()
                }
            })
//            Observer { barangBekas ->
//                barangBekas?.let {
//                    this.barangBekas = barangBekas
//                    photoFile = barangBekasDetailViewModel.getPhotoFile(barangBekas)
//                    photoUri = FileProvider.getUriForFile(
//                        requireActivity(),
//                        "com.bignerdranch.android.criminalintent.fileprovider",
//                        photoFile
//                    )
//                    updateUI()
//                }
//            }
//        )
    }

    companion object {
        fun newInstance(barangBekasId: UUID): BarangBekasFragment {
            val args = Bundle().apply {
                putSerializable(ARG_BARANG_BEKAS_ID, barangBekasId)
            }
            return BarangBekasFragment().apply {
                arguments = args
            }
        }
    }
}