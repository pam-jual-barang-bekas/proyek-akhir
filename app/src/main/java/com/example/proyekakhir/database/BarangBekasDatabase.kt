package com.example.proyekakhir.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.proyekakhir.BarangBekas

@Database(entities = [BarangBekas::class], version = 2)
@TypeConverters(BarangBekasTypeConverters::class)
abstract class BarangBekasDatabase : RoomDatabase(){
    abstract fun barangBekasDao(): BarangBekasDao
}
