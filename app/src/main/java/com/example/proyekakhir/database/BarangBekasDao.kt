package com.example.proyekakhir.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.example.proyekakhir.BarangBekas
import java.util.*

@Dao
interface BarangBekasDao {
    @Query("SELECT * FROM barangBekas")
//    fun getBarangBekass(): List<BarangBekas>
    fun getBarangBekass(): LiveData<List<BarangBekas>>

    @Query("SELECT * FROM barangBekas WHERE id=(:id)")
//    fun getBarangBekas(id: UUID): BarangBekas?
    fun getBarangBekas(id: UUID): LiveData<BarangBekas?>

    @Update
    fun updateBarangBekas(barangBekas: BarangBekas)

    @Insert
    fun addBarangBekas(barangBekas: BarangBekas)
}