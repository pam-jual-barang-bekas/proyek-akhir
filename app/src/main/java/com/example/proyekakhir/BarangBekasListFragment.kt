package com.example.proyekakhir

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.io.File
import java.util.*

private const val TAG = "BarangBekasListFragment"

class BarangBekasListFragment : Fragment() {

    interface Callbacks {
        fun onBarangBekasSelected(barangBekasId: UUID)
    }

    private var callbacks: Callbacks? = null

    private lateinit var barangBekasRecyclerView: RecyclerView
//    private var adapter: BarangBekasAdapter? = null
    private var adapter: BarangBekasAdapter? = BarangBekasAdapter(emptyList())

    private val barangBekasListViewModel: ListViewModel by lazy {
        ViewModelProviders.of(this).get(ListViewModel::class.java)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        callbacks = context as Callbacks?
    }
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        Log.d(TAG, "Total crimes: ${barangBekasListViewModel.barangBekass.size}")
//    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        //Log.d(TAG,"Total crimes: ${crimeListViewModel.crimes.size}")
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.fragment_barang_bekas_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.new_barang_bekas -> {
                val barangBekas = BarangBekas()
                barangBekasListViewModel.addBarangBekas(barangBekas)
                callbacks?.onBarangBekasSelected(barangBekas.id)
                true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_barang_bekas_list, container, false)
        barangBekasRecyclerView = view.findViewById(R.id.barang_bekas_recycler_view) as RecyclerView
        barangBekasRecyclerView.layoutManager = LinearLayoutManager(context)
        barangBekasRecyclerView.adapter = adapter

//        updateUI()
        return view
    }

    private val barangBekasDetailViewModel: BarangBekasDetailViewModel by lazy {
        ViewModelProviders.of(this).get(BarangBekasDetailViewModel::class.java)
    }

    private inner class BarangBekasHolder(view: View) : RecyclerView.ViewHolder(view),
        View.OnClickListener {
        lateinit var barangBekas: BarangBekas
        private lateinit var photoFile: File

        val namaBarangTextView: TextView = itemView.findViewById(R.id.nama_barang)
        val detailTextView: TextView = itemView.findViewById(R.id.detail_barang)
        private val soldImageView: ImageView = itemView.findViewById(R.id.barang_bekas_sold)
        private val photoView: ImageView = itemView.findViewById(R.id.img_show)

        init {
            itemView.setOnClickListener(this)
        }

        fun bind(barangBekas: BarangBekas) {
            this.barangBekas = barangBekas
            photoFile = barangBekasDetailViewModel.getPhotoFile(barangBekas)
            val bitmap = getScaledBitmap(photoFile.path, requireActivity())
            photoView.setImageBitmap(bitmap)
            this.barangBekas = barangBekas
            namaBarangTextView.text = this.barangBekas.nama_barang
            detailTextView.text = this.barangBekas.detail_barang
            soldImageView.visibility = if (barangBekas.habis){
                View.VISIBLE
            } else {
                View.GONE
            }
        }

        override fun onClick(v: View?) {
//            Toast.makeText(context, "${barangBekas.nama_barang} pressed!", Toast.LENGTH_SHORT)
//                .show()
            callbacks?.onBarangBekasSelected(barangBekas.id)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        barangBekasListViewModel.barangBekassListLiveData.observe(
            viewLifecycleOwner,
            Observer { barangBekass ->
                barangBekass?.let{
                    Log.i(TAG, "Got barangBekass ${barangBekass.size}")
                    updateUI(barangBekass)
                }
            }
        )
    }

    override fun onDetach() {
        super.onDetach()
        callbacks = null
    }

//    private fun updateUI() {
    private fun updateUI(barangBekass: List<BarangBekas>) {
//        val barangBekass = barangBekasListViewModel.barangBekass
        adapter = BarangBekasAdapter(barangBekass)
        barangBekasRecyclerView.adapter = adapter
    }

    companion object {
        fun newInstance(): BarangBekasListFragment {
            return BarangBekasListFragment()
        }
    }

    private inner class BarangBekasAdapter(var barangBekass: List<BarangBekas>) :
        RecyclerView.Adapter<BarangBekasHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
                : BarangBekasHolder {
            val view = layoutInflater.inflate(R.layout.list_item_barang_bekas, parent, false)
            return BarangBekasHolder(view)
        }

        override fun getItemCount() = barangBekass.size

        override fun onBindViewHolder(holder: BarangBekasHolder, position: Int) {
            val barangBekas = barangBekass[position]
//            holder.apply {
//                namaBarangTextView.text = barangBekas.nama_barang
//                detailTextView.text = barangBekas.detail_barang

            holder.bind(barangBekas)
        }
    }


}