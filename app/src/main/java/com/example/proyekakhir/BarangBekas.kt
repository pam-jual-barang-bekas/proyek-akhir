package com.example.proyekakhir

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class BarangBekas (@PrimaryKey val id:UUID = UUID.randomUUID(),
                        var nama_barang: String = "",
                        var detail_barang: String = "",
                        var habis: Boolean = false
)

{
    val photoFileName
        get() = "IMG_$id.jpg"
}