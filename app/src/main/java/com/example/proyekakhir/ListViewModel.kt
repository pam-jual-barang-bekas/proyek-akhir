package com.example.proyekakhir

import androidx.lifecycle.ViewModel

class ListViewModel : ViewModel() {
//    val barangBekass = mutableListOf<BarangBekas>()
//
//    init {
//        for(i in 0 until 100){
//            val barangBekas = BarangBekas()
//            barangBekas.nama_barang = "Crime #$i"
//            barangBekas.habis = i % 2 == 0
//            barangBekass += barangBekas
//        }
//    }

    private val barangBekasRepository = BarangBekasRepository.get()
//        val barangBekass = barangBekasRepository.getBarangBekass()
    val barangBekassListLiveData = barangBekasRepository.getBarangBekass()

    fun addBarangBekas(barangBekas: BarangBekas) {
        barangBekasRepository.addBarangBekas(barangBekas)
    }
}
