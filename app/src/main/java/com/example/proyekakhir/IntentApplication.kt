package com.example.proyekakhir

import android.app.Application

class IntentApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        BarangBekasRepository.initialize(this)
    }
}