package com.example.proyekakhir

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.Room
import com.example.proyekakhir.database.BarangBekasDatabase
import java.io.File
import java.util.*
import java.util.concurrent.Executors

private const val DATABASE_NAME = "crime-database"

class BarangBekasRepository private constructor(context: Context) {
    private val database: BarangBekasDatabase =
        Room.databaseBuilder(
            context.applicationContext,
            BarangBekasDatabase::class.java,
            DATABASE_NAME
        ).build()
    private val barangBekasDao = database.barangBekasDao()
    private val executor = Executors.newSingleThreadExecutor()
    private val filesDir = context.applicationContext.filesDir
//
//        fun getBarangBekass(): List<BarangBekas> = barangBekasDao.getBarangBekass()
    fun getBarangBekass(): LiveData<List<BarangBekas>> = barangBekasDao.getBarangBekass()

//        fun getBarangBekas(id: UUID): BarangBekas? = barangBekasDao.getBarangBekas(id)
    fun getBarangBekas(id: UUID): LiveData<BarangBekas?> = barangBekasDao.getBarangBekas(id)

    fun updateBarangBekas(barangBekas: BarangBekas) {
        executor.execute {
            barangBekasDao.updateBarangBekas(barangBekas)
        }
    }

    fun addBarangBekas(barangBekas: BarangBekas) {
        executor.execute {
            barangBekasDao.addBarangBekas(barangBekas)
        }
    }

    fun getPhotoFile(barangBekas: BarangBekas): File = File(filesDir, barangBekas.photoFileName)

    companion object{
        private var INSTANCE: BarangBekasRepository? = null

        fun initialize(context: Context){
            if (INSTANCE == null){
                INSTANCE = BarangBekasRepository(context)
            }
        }

        fun get(): BarangBekasRepository{
            return INSTANCE?:
            throw IllegalStateException("BarangBekasRepository must be initialized")
        }
    }
}